﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ASP.NETCore.Identity
{
    public class UserDbContext : IdentityDbContext<User>
    {
        public UserDbContext(DbContextOptions<UserDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // before we call anything cutom, we call the base
            base.OnModelCreating(builder);

            // Add another tabnle to the database
            builder.Entity<User>(user => user.HasIndex(x => x.Locale).IsUnique(false));

            // Setup how the organization object is treated
            builder.Entity<Organization>(org =>
            {
                // Organizations are stored in a table called Organizations
                org.ToTable("Organizations");

                // The key is the Id property
                org.HasIndex(x => x.Id);

                // An organization has many users and each user has one organization
                // The user have a foreign key called OrgId which is not required
                org.HasMany<User>().WithOne().HasForeignKey(x => x.OrgId).IsRequired(false);
            });
        }
    }
}
