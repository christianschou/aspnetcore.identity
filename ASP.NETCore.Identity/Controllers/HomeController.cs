﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASP.NETCore.Identity.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace ASP.NETCore.Identity.Controllers
{
    public class HomeController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IUserClaimsPrincipalFactory<User> _claimsPrincipalFactory;
        private readonly SignInManager<User> _signInManager;

        public HomeController(UserManager<User> userManager,
            IUserClaimsPrincipalFactory<User> claimsPrincipalFactory,
            SignInManager<User> signInManager)
        {
            this._userManager = userManager;
            _claimsPrincipalFactory = claimsPrincipalFactory;
            _signInManager = signInManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [Authorize]
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // This will return null, if the user doesn't exist
                var user = await _userManager.FindByNameAsync(model.UserName);

                if (user == null)
                {
                    user = new User
                    {
                        // Using Guid to give it some sort of unique value
                        // we dont set the normalized username, we will rely on the user manager for that
                        Id = Guid.NewGuid().ToString(),
                        UserName = model.UserName
                    };

                    // When we create the user, we are also gonna pass in the password
                    // Again we will rely on the create method
                    var result = await _userManager.CreateAsync(user, model.Password);
                }

                return View("Success");
            }

            return View();
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                // Does the user exists?
                var user = await _userManager.FindByNameAsync(model.UserName);

                // Lets check their password, but we got the plain text version
                // of their password, but we only have the hashed version stored.
                // This means we need the userManager to check the passwords using its
                // password hasher, which we can do using the CheckPasswordAsync method.
                if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
                {
                    // If evertyhing is correct, we will login the user

                    //// We create a new claimsidentity using the default ASP.NET Identity schema
                    //var identity = new ClaimsIdentity("Identity.Application");

                    //// Lets add a couple of claims to that identity (their unique identifier and their name)
                    //identity.AddClaim(new Claim(ClaimTypes.NameIdentifier, user.Id));
                    //identity.AddClaim(new Claim(ClaimTypes.Name, user.UserName));

                    var principal = await _claimsPrincipalFactory.CreateAsync(user);

                    // Lets wrap this in a ClaimsPrinicple and call the SignIn method
                    await HttpContext.SignInAsync("Identity.Application", principal);

                    // If we got this far, we can redirect the user back to the homepage
                    return RedirectToAction("Index");
                }

                // If they didn't get to login, their credentials are incorrect
                // Lets give them a dynamic error message
                ModelState.AddModelError("", "Invalid Username or Password");
            }

            // Finally return them back to our view
            return View();
        }
    }
}
